import App from './assets/App';

function ready() {
    let button = document.getElementById('button');
    let canvas = document.getElementById('canvas');
    let app = new App(canvas, '2020', '60px Roboto');

    app.init();

    button.addEventListener('click', (e) => {
        e.preventDefault();
        let target = e.target;

        target.disabled = true;

        app.animate(() => {
            target.disabled = false;
            console.log('animation complete');
        });
    }, false)
}

document.addEventListener("DOMContentLoaded", ready);

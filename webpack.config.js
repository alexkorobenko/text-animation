let webpack = require('webpack');

function getPlugins() {
    let plugins = [];

    plugins.push(new webpack.optimize.UglifyJsPlugin());

    return plugins;
};

module.exports = {
    entry: './index.js',
    output: {
        path: __dirname + '/build/',
        publicPath: 'build/',
        filename: 'index.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015'],
                },
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ['.js'],
        modules: [
            'node_modules'
        ]
    },
    plugins: getPlugins()
};

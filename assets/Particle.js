import { getRandomArbitrary } from './utils';

export default class Particle {
  constructor(randomX, randomY, x, y, radius = 1, color = 'black') {
    //const
    this.randomX = randomX;
    this.randomY = randomY;
    this.x = x;
    this.y = y;

    this.radius = radius;
    this.color = color;
    this.speed = getRandomArbitrary(1, 1.5).toFixed(1);

    //dynamic
    this.startX = null;
    this.startY = null;
    this.endX = null;
    this.endY = null;
    this.distX = null;
    this.distY = null;

    this.currentX = null;
    this.currentY = null;
  }

  calc() {
    this.currentX = this.startX;
    this.currentY = this.startY;

    this.distX = this.endX - this.startX;
    this.distY = this.endY - this.startY;
  }

  setIn() {
    this.startX = this.x;
    this.startY = this.y;
    this.endX = this.randomX;
    this.endY = this.randomY;

    this.calc();
  }

  setOut() {
    this.startX = this.randomX;
    this.startY = this.randomY;
    this.endX = this.x;
    this.endY = this.y;

    this.calc();
  }

  update(k) {
    //k: from 0 to 1
    let dX = this.distX * k * this.speed;
    let dY = this.distY * k * this.speed;

    if (Math.abs(dX) >= Math.abs(this.distX)) {
      dX = this.distX;
    }

    if (Math.abs(dY) >= Math.abs(this.distY)) {
      dY = this.distY;
    }

    this.currentX = this.startX + dX;
    this.currentY = this.startY + dY;
  }
}

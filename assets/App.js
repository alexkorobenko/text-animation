import { TweenMax, Circ } from "gsap";
import Particle from './Particle';
import * as utils from './utils';

export default class App {
    constructor(canvas, text = 'hello', font = '60px Arial') {
        this.state = 1;
        this.textData = {};
        this.points = [];
        this.particles = [];
        this.canvas = canvas;
        this.ctx = canvas.getContext('2d');
        this.width = null;
        this.height = null;
        this.text = text;
        this.font = font;
    }

    init() {
        this.resize();

        //create particles
        this.textData = utils.convertTextToData(this.text, this.font); //convert text to data
        this.points = this.getScalePoints(); //scale points from image data to new canvas
        this.particles = this.getParticles(); //create particles

        //draw particles
        this.clear();
        this.reverseParticles();
        this.drawParticles();
    }

    getScalePoints() {
        let points = [];
        let kX = this.width / this.textData.imageData.width;
        let kY = this.height / this.textData.imageData.height;
        let k = utils.getMinFromArray([kX, kY]);
        let offsetX = (this.width / 2) - (this.textData.imageData.width * k / 2);
        let offsetY = (this.height / 2) - (this.textData.imageData.height * k / 2);

        this.textData.data.forEach(point => {
            let x = (point.x * k) + offsetX;
            let y = (point.y * k) + offsetY;

            points.push({
                'x': x,
                'y': y
            });
        });

        return points;
    }

    getParticles() {
        let particles = [];

        this.points.forEach(el => {
            let randomX = utils.getRandomInt(0, this.width);
            let randomY = utils.getRandomInt(0, this.height);
            let x = el.x;
            let y = el.y;

            particles.push( new Particle(randomX, randomY, x, y, 1, 'orange') );
        });

        return particles;
    }

    drawPoint(el) {
        this.ctx.beginPath();
        this.ctx.arc(el.currentX, el.currentY, 1, 0, 2 * Math.PI);
        this.ctx.closePath();
        this.ctx.fill();
    }

    drawParticles() {
        this.particles.forEach(el => this.drawPoint(el));
    }

    reverseParticles() {
        this.particles.forEach(el => {
            this.state ? el.setIn() : el.setOut();
        });
    }

    animate(callback) {
        let counter = {index: 0};
        let onComplete = false;

        TweenMax.to(counter, 3, {
            index: 1,
            onUpdate: () => {
                if (!onComplete) {
                    let k = counter.index;

                    this.clear();

                    this.particles.forEach(el => {
                        el.update(k);
                        this.drawPoint(el);
                    });

                    if (k === 1) {
                        onComplete = true;
                        this.state = !this.state;
                        this.reverseParticles();
                        callback && callback();
                    }
                }
            },
            ease: Circ.easeOut
        });
    }

    clear() {
        this.ctx.clearRect(0, 0, this.width, this.height);
    }

    resize() {
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.canvas.width = this.width;
        this.canvas.height = this.height;
    }
}

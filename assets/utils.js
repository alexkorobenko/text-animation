export function convertTextToData(text, font) {
  let buffer = document.createElement('canvas');
  let bufferCtx = buffer.getContext('2d');
  let bufferWidth = bufferCtx.canvas.width;
  let bufferHeight = bufferCtx.canvas.height;

  bufferCtx.font = font;

  //document.body.appendChild(buffer);

  let textWidth = bufferCtx.measureText(text).width;
  let textHeight = parseInt(bufferCtx.font.match(/\d+/), 10);

  //draw text in buffer canvas and centered text in canvas
  bufferCtx.fillStyle = 'orange';
  bufferCtx.fillText(text , (bufferWidth/2) - (textWidth/2), (bufferHeight/2) + (textHeight/1.5/2));

  let data = [];
  let imageData = bufferCtx.getImageData(0, 0, bufferWidth, bufferHeight); //get array of all pixels canvas
  let pixels = imageData.data;
  let l = bufferWidth * bufferHeight;

  for (let i = 0; i < l; i+=1) {
    // get color of pixel
    //let r = pixels[i*4]; // Red
    //let g = pixels[i*4+1]; // Green
    //let b = pixels[i*4+2]; // Blue
    let a = pixels[i*4+3]; // Alpha

    if (a > 0) {
      let y = parseInt(i / bufferWidth, 10);
      let x = i - y * bufferWidth;

      data.push({
        'x': x,
        'y': y
      });
    }
  }

  return {
    'imageData': imageData, //all pixels of buffer canvas
    'data': data, //only color pixels, alpha > 1
  };
}

export function getRandomFromArray(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

export function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

export function getMinFromArray(arr) {
  return Math.min.apply(null, arr);
}

export function getMaxFromArray(arr) {
  return Math.max.apply(null, arr);
}
